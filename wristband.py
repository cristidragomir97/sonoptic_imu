import rospy, sys, struct, time, math , threading, subprocess
from bluepy.btle import UUID, Peripheral,DefaultDelegate
from geometry_msgs.msg import Vector3, TransformStamped, Transform, Quaternion
from sensor_msgs.msg import Imu, MagneticField
from tf2_ros import TransformBroadcaster
from tf.transformations import *
import numpy as np 

from colorama import Fore

def ImuFilter(fixed_frame):
    
    command = [
        "/opt/ros/noetic/bin/rosrun",
        "imu_complementary_filter",
        "complementary_filter_node",
        "_fixed_frame:={}".format(fixed_frame),
        "_use_mag:=true",
        "_do_bias_estimation:=true", 
        "_do_adaptive_gain:=true"]
    
    filter_pid = subprocess.Popen(command, stdout=subprocess.PIPE,).pid
    print(Fore.GREEN + "[*] IMU Filter started")

class BLEDelegate(DefaultDelegate):

    def __init__(self, params):
        DefaultDelegate.__init__(self)
        self.raw_pub = rospy.Publisher("/imu/data_raw", Imu, queue_size=100)
        self.mag_pub = rospy.Publisher("/imu/mag", MagneticField, queue_size=10)
    
    def make_message(self, float_list):
        #print(float_list)
        mx, my, mz, ax, ay, az, gx, gy, gz = float_list
        
        raw_msg = Imu()
        raw_msg.header.frame_id = "hand"
        raw_msg.header.stamp = rospy.get_rostime()

        raw_msg.angular_velocity.x = gx * math.pi / 180.0
        raw_msg.angular_velocity.y = gy * math.pi / 180.0
        raw_msg.angular_velocity.z = gz * math.pi / 180.0
        
        raw_msg.linear_acceleration.x = ax * math.pi / 180.0
        raw_msg.linear_acceleration.y = ay * math.pi / 180.0
        raw_msg.linear_acceleration.z = az * math.pi / 180.0

        mag_msg = MagneticField()
        mag_msg.header.frame_id = "hand"
        mag_msg.header.stamp = rospy.get_rostime()

        mag_msg.magnetic_field.x = mx
        mag_msg.magnetic_field.y = my
        mag_msg.magnetic_field.z = mx


        return mag_msg, raw_msg


    def handleNotification(self, cHandle, data):
        line = data.decode().split(",")
        global calib_flag
        calib_flag = line[0]

        ## check if calibration message
        float_list = list(map(float, line[1::]))
        mag_msg, raw_msg = self.make_message(float_list)
        
        self.raw_pub.publish(raw_msg)
        self.mag_pub.publish(mag_msg)

class WristReceiver(threading.Thread):

    def __init__(self, address="F7:B5:D5:1B:2F:38"):
        calib_flag = "OFF"
        print(Fore.GREEN + "[*] WristReceiver started")
        service_id = UUID("590d65c7-3a0a-4023-a05a-6aaf2f22441c")
        char_id = UUID("00000001-0000-1000-8000-00805f9b34fb")
        device = Peripheral(address)

        service = device.getServiceByUUID(service_id)
        characteristic = service.getCharacteristics(char_id)[0]

        #time.sleep(0.1)
        device.setMTU(244)

        # get handle and turn notifications on 
        handle = device.getDescriptors(characteristic.getHandle(),0x0C)[0].handle

        device.writeCharacteristic(characteristic.valHandle+1, struct.pack('<bb', 0x02, 0x00))
        
        delegate = BLEDelegate(device)
        device.setDelegate(delegate)

        self.device = device
        self.pid = ImuFilter("headset")
        
        threading.Thread.__init__(self)
    


    def run(self):
    
        while True:
            if self.device.waitForNotifications(0.01):
                continue

class WristBroadcaster(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        
        print(Fore.GREEN + "[*] WristBroadcaster started")
        self.origin = [0.00, 0.5, 0.00]
        self.tb = TransformBroadcaster()
        self.d = quaternion_from_euler(0, 0, 0)
        self.r = quaternion_from_euler(0, 0, math.pi) # sets correct x,y,z
    
        rospy.Subscriber("/imu/data", Imu, self.callback)
        rospy.spin()

    def m_to_q(self, m, inverse=False):
        if inverse:
            return [m.orientation.x, m.orientation.y, m.orientation.z, -m.orientation.w]
        else:
            return [m.orientation.x, m.orientation.y, m.orientation.z, m.orientation.w]

        

    def send_tf(self, q, name, delta):
        t = TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "headset"
        t.child_frame_id = name
        


        t.transform.translation.x  = self.origin[0]
        t.transform.translation.y  = self.origin[1] + delta
        t.transform.translation.z  = self.origin[2]
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]      
        self.tb.sendTransform(t)

    def magic_transform(self,q):
        return q[1], q[2], q[3], q[0]


    def callback(self, m):
        global calib_flag

        if(calib_flag == "ON"):
            d = self.m_to_q(m)
            self.d = [d[0], d[1], d[2], -d[3]]
            print("\t * setting current point as origin ...")
        
        else:
            q = self.m_to_q(m, False)        
            q = quaternion_multiply(self.d, q) 
            self.send_tf(q, "headset", 1)






   

    

    
        

    
    
  