from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, TransformStamped, Transform
from tf2_ros import TransformBroadcaster

import rospy, math, bleak, platform, logging, asyncio, threading
from bleak import BleakClient
from bleak import BleakClient
from bleak import _logger as logger
from bleak.uuids import uuid16_dict

UART_TX_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e" #Nordic NUS characteristic for TX
UART_RX_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca9e" #Nordic NUS characteristic for RX

dataFlag = False 

class Wristband(threading.Thread):

    def broadcastTF(self, q):
        t = TransformStamped()

        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "world"
        t.child_frame_id = self.frame_id
        
        #translation = (self.origin[0], self.origin[1], self.origin[2])
        
        t.transform.translation.x  = self.origin[0]
        t.transform.translation.y  = self.origin[1]
        t.transform.translation.z  = self.origin[2]

        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]
        
        #self.tb.sendTransform(translation, rotation, time, self.frame_id, 'base_link')
        self.tb.sendTransform(t)

    def imuMessage(self, _list):
        msg = Imu()
        msg.header.frame_id = self.frame_id
        msg.header.stamp = rospy.get_rostime()

        msg.orientation.w = _list[0]
        msg.orientation.x = _list[1]
        msg.orientation.y = _list[2]
        msg.orientation.z = _list[3]
        msg.angular_velocity.x = _list[4]
        msg.angular_velocity.y = _list[5]
        msg.angular_velocity.z = _list[6]
        msg.linear_acceleration.x = _list[7]
        msg.linear_acceleration.y = _list[8]
        msg.linear_acceleration.z = _list[9]

        self.pub.publish(msg)

    def notification_handler(self, sender, data):
        raw = list(data.decode("utf-8").split(","))
        conv = list(map(lambda x: float(x), raw))
        
        self.imuMessage(conv)
        self.broadcastTF(conv)

        global dataFlag
        dataFlag = True

    def __init__(self, address, origin, topic, frame):
        rospy.init_node('relay_hand', anonymous=False)
        self.tb = TransformBroadcaster()

        threading.Thread.__init__(self)
        print("* initialized wristband IMU")
       
        self.origin = origin
        self.topic = topic
        self.frame_id = frame

        self.pub = rospy.Publisher(self.topic, Imu, queue_size=100)
        rate = rospy.Rate(30)

        address = (address)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.run(address, loop))
  

    async def run(self, address, loop):
        async with BleakClient(address, loop=loop) as client:

            #wait for BLE client to be connected
            x = await client.is_connected()
            print("Connected: {0}".format(x))

            #wait for data to be sent from client
            await client.start_notify(UART_RX_UUID, self.notification_handler)


            while True : 

                #give some time to do other tasks
                await asyncio.sleep(0.01)

                #check if we received data
                global dataFlag
                if dataFlag :
                    dataFlag = False

                    #echo our received data back to the BLE device
                    data = await client.read_gatt_char(UART_RX_UUID)
                    await client.write_gatt_char(UART_TX_UUID,data)



if __name__ == "__main__": 
    ws = Wristband("E2:BF:2D:EA:D8:58", [1.0, 1.50, -0.80], "/hand/imu/data", "hand_imu")
    
