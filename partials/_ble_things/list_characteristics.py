import sys
from bluepy.btle import UUID, Peripheral


p = Peripheral("F7:B5:D5:1B:2F:0C")

chList = p.getCharacteristics()
                    
for ch in chList:
   print ("  0x"+ format(ch.getHandle(),'02X')  +"   "+str(ch.uuid) +" " + ch.propertiesToString())
