import sys
import binascii
import struct
import time
from bluepy.btle import UUID, Peripheral

button_service_uuid = UUID("590d65c7-3a0a-4023-a05a-6aaf2f22441c")
button_char_uuid    = UUID("00000001-0000-1000-8000-00805f9b34fb")


p = Peripheral("F7:B5:D5:1B:2F:0C")
ButtonService=p.getServiceByUUID(button_service_uuid)

try:
    ch = ButtonService.getCharacteristics(button_char_uuid)[0]
    if (ch.supportsRead()):
        while 1:
            start_time = time.time() # start time of the loop

            print (ch.read())
            
            print("FPS: ", 1.0 / (time.time() - start_time)) # FPS = 1 / time to process loop
            time.sleep(0.01)


finally:
    p.disconnect()