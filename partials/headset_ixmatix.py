import rospy, serial, threading, math
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, TransformStamped, Transform
from tf import TransformBroadcaster


class Headset(threading.Thread):

    def broadcast(self, q):
        time = rospy.Time.now()
        translation = (self.origin[0], self.origin[1], self.origin[2])
        rotation = (q[0], q[1], q[2], q[3])
        self.tb.sendTransform(translation, rotation, time, self.frame_id, 'base_link')

    def __init__(self, address, origin, topic, frame):
        rospy.init_node('relay_head', anonymous=True)
        self.tb = TransformBroadcaster()

        print('* initialized headset imu')
        self.serial = serial.Serial(address, 112500, timeout=1)
        self.pub = rospy.Publisher(topic, Imu, queue_size=1000)

      
        self.frame_id = frame
        self.origin = origin

        rate = rospy.Rate(30)

        self.gRes = 6.1e-05
        self.aRes = 0.00875
        self.mRes = 0.00875

        self.gOff = [-0.05, 0.35, -0.73]
        self.aOff = [0.04, -0.1, -0.01]
        self.mOff = [-0.00563, 0.01678, -0.0068]

        self.soft_iron = [0.99, -0.002, -0.005, -0.002, 0.989, -0.075, -0.005, -0.075, 1.027]

        threading.Thread.__init__(self)

    def processParam(self, line):
            if(len(line) == 2):
                # process resolution message 
                if (line[0] == "aRes:"):
                    self.aRes = float(line[1])
                    print("* Accel. Resolution: {}".format(self.aRes))
                elif (line[0] == "gRes:"):
                    self.gRes = float(line[1])
                    print("* Gyro Resolution: {}".format(self.gRes))
                elif (line[0] == "mRes:"):
                    self.mRes =  float(line[1])
                    print("* Mag. Resolution: {}".format(self.mRes))

            elif(len(line) == 4):
                # process accel and gyro offsets
                if (line[0] == "aOff:"):
                    self.aOff = [float(line[1]), float(line[2]), float(line[3])]
                    print("* Accel. Offset: ", self.aOff)
                elif(line[0] == "gOff:"):
                    self.gOff = [float(line[1]), float(line[2]), float(line[3])]
                    print("* Gyro Offset: ", self.gOff)

            
            elif(len(line) == 13):
                if (line[0] == "mOff:"):
                    #process magnetic offset 
                    self.mOff = [float(line[1]), float(line[2]), float(line[3])]
                    print("* Mag: Offset: ", self.mOff)

                    # process softiron matrix
                    self.soft_iron = []
                    k = 4

                    for i in range(0,3):
                        for j in range(0,3):
                            self.soft_iron.append(float(line[k]))
                            k+=1

                    print("* SoftIron:  ", self.soft_iron)

    def processUpdate(self, line):
    
        msg = Imu()

        
  
            msg.header.frame_id = self.frame_id
            msg.header.stamp = rospy.get_rostime()
            print(line)

            q0, q1, q2, q3 =  float(line[1]),  float(line[2]),  float(line[3]),  float(line[4])

            msg.orientation.w = q0
            msg.orientation.x = q1
            msg.orientation.y = q2
            msg.orientation.z = q3

            msg.angular_velocity.x = (int(line[5]) *  self.gRes - self.gOff[0]) * math.pi / 180.0
            msg.angular_velocity.y = (float(line[6]) *  self.gRes - self.gOff[1]) * math.pi / 180.0
            msg.angular_velocity.z = (float(line[7]) *  self.gRes - self.gOff[2]) * math.pi / 180.0

            msg.angular_velocity_covariance[0] = float(line[8])
            msg.angular_velocity_covariance[4] = float(line[9])
            msg.angular_velocity_covariance[8] = float(line[10])

            msg.linear_acceleration.x = (float(line[11]) *  self.aRes - self.aOff[0]) * math.pi / 180.0
            msg.linear_acceleration.y = (float(line[12]) *  self.aRes - self.aOff[1]) * math.pi / 180.0
            msg.linear_acceleration.z = (float(line[13]) *  self.aRes - self.aOff[2]) * math.pi / 180.0

            msg.linear_acceleration_covariance[0] = float(line[14])
            msg.linear_acceleration_covariance[4] = float(line[15])
            msg.linear_acceleration_covariance[8] = float(line[16])

            self.pub.publish(msg)
            return q0, q1, q2, q3

    def toggleVib(self, channel, on): 
        if on:
            self.serial.write(bytes("{},1\n".format(channel), encoding='utf8'))
        else:
            self.serial.write(bytes("{},0\n".format(channel), encoding='utf8'))

    def setVib(self, channel, timing, duty):
        string = "{},u,{}".format(channel, len(timing))
        for x in timing: string += "{}, ".format(int(x))
        string += "{},".format(len(duty))
        for x in duty: string += "{}, ".format(int(x))
        string += "\n"

        self.serial.write(bytes(string, encoding='utf8'))
        
    def run(self):
        while True:
            line = str(self.serial.readline().decode()).split(' ')
                
            if(len(line) == 17):
                q = self.processUpdate(line)
                self.broadcast(q)
            else: 
                self.processParam(line)


if __name__ == "__main__":
    hs = Headset("/dev/ttyS1", [0.0, 0.0, 0.20], "/imu/data_raw", "head_imu")
    hs.start()

   
