import rospy, time, math, serial, signal, sys, tf2_ros
import numpy as np 

from threading import Thread
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, TransformStamped, Transform, Quaternion
from tf2_ros import TransformBroadcaster
from tf.transformations import *

def signal_handler(sig, frame):
    sys.exit(0)


ORIGIN = [0.00, 0.5, 0.00]
TOPIC = "/imu/head_raw"



serial = serial.Serial("/dev/ttyS1", 112500, timeout=1)

rospy.init_node('headset_imu', anonymous=False)
rate = rospy.Rate(30)

pub = rospy.Publisher(TOPIC, Imu, queue_size=100)

tb =  tf2_ros.TransformBroadcaster()
tfBuffer = tf2_ros.Buffer()
listener = tf2_ros.TransformListener(tfBuffer)


def split_line(line):
    return str(line).replace('\r','').replace('\n','').split(',')

def send_tf(q, name):
    t = TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "base_link"
    t.child_frame_id = name
    
    t.transform.translation.x  = ORIGIN[0]
    t.transform.translation.y  = ORIGIN[1] 
    t.transform.translation.z  = ORIGIN[2]
    t.transform.rotation.x = q[0]
    t.transform.rotation.y = q[1]
    t.transform.rotation.z = q[2]
    t.transform.rotation.w = q[3]        
    tb.sendTransform(t)


def get_q():
    line = serial.readline().decode()
    line = split_line(line)
    line = map(float, line)
    qw, qx, qy, qz, la_x, la_y, la_z, gr_x, gr_y, gr_z, bat = line
    q = [qx, qy, qz, qw]
    
    return  q / np.linalg.norm(q)
   

def send_battery_message(bat):
    pass

def sample(n):
    for i in range(0, n):
        try:
            send_tf(get_q(), "original")
            time.sleep(0.01)

        except ValueError:
            print("* Misformated message, skiping")

    t = tfBuffer.lookup_transform('base_link', 'original',  rospy.Time(0))
    dx = t.transform.rotation.x 
    dy = t.transform.rotation.y 
    dz = t.transform.rotation.z 
    dw = t.transform.rotation.w 
    return [dx, dy, dz, -dw]

def magic_transform(q):
    return q[1], q[2], q[3], q[0]
    

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)

    print("* Sampling... ")
    d = sample(10)
    r = quaternion_from_euler(0, math.pi, math.pi) # sets correct x,y,z

    print("* Calibration done. Starting loop.. ")
    while True:
        q = get_q()
        q = quaternion_multiply(d, q)
        q = quaternion_multiply(r, q)
        q = magic_transform(q) # no clue what it does but it works
        send_tf(q, 'headset')



   
    

  


   
