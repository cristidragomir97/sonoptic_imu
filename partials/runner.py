import subprocess, colorama, sys, signal, time, threading
from colorama import Fore, Style

pids = []
FIXED = "headset"


def signal_handler(sig, frame):
    for pid in pids:
        print(Fore.RED + "Killing {}...".format(pid))
        kill_line = "kill {}".format(pid)
        subprocess.call(kill_line, shell=True)
    
    sys.exit(0)

def _handler(p,t):
    while True:
        line = p.communicate()[0]
        if line != b'':
            print("[{}]>".format(t), )
    


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)

    filter_process = subprocess.Popen(["/opt/ros/noetic/bin/rosrun", "imu_complementary_filter","complementary_filter_node",
                                    "_fixed_frame:={}".format(FIXED), "_use_mag:=false", "_do_bias_estimation:=true", 
                                    "_do_adaptive_gain:=false"], stdout=subprocess.PIPE,)
    #threading.Thread(target=_handler, args=(filter_process, "filter_thread")).start()

    pids.append(filter_process.pid)

    print(Fore.BLUE+ "[*] Wrist IMU Filter started... ")

    time.sleep(1.5)

 
    headset_process = subprocess.Popen(["/usr/bin/python3","/home/uc0de/code/imu/headset.py"], stdout=subprocess.PIPE,)
    #threading.Thread(target=_handler, args=(headset_process, "headset_thread")).start()

    pids.append(headset_process.pid)
    time.sleep(0.5)

    print(Fore.CYAN + "[*] Head reciever started... ")

    wristband_process = subprocess.Popen(["/usr/bin/python3","/home/uc0de/code/imu/wrist.py"], stdout=subprocess.PIPE,)
    #threading.Thread(target=_handler, args=(wristband_process, "wristband_thread")).start()
    pids.append(wristband_process.pid)

    print(Fore.CYAN + "[*] Wristrecevier started... ")

    


    twist = subprocess.Popen(["/usr/bin/python3","/home/uc0de/code/imu/wrist_tf.py"], stdout=subprocess.PIPE,)
    #threading.Thread(target=_handler, args=(wristband_process, "wristband_thread")).start()
    pids.append(twist.pid)
    print(Fore.YELLOW + "[*] Wrist TF Fixer started... ")

    time.sleep(1.5)

    signal.pause()


