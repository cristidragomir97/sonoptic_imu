import rospy, time, math, serial
import numpy as np 

from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, TransformStamped, Transform
from tf2_ros import TransformBroadcaster


ORIGIN = [1.0, 1.50, -0.80]
TOPIC = "/imu/data_raw"

serial = serial.Serial("/dev/ttyS1", 112500, timeout=1)

rospy.init_node('headset_imu', anonymous=False)
rate = rospy.Rate(30)

pub = rospy.Publisher(TOPIC, Imu, queue_size=1000)

def split_line(line):
    return str(line).replace('\r','').replace('\n','').split(',')

def send_imu_msg(ax, ay, az, gx, gy, gz):
    msg = Imu()
    msg.header.frame_id = "head_imu"
    msg.header.stamp = rospy.get_rostime()
    
    msg.angular_velocity.x = gx * math.pi / 180.0
    msg.angular_velocity.y = gy * math.pi / 180.0
    msg.angular_velocity.z = gz * math.pi / 180.0
    
    msg.linear_acceleration.x = ax * math.pi / 180.0
    msg.linear_acceleration.y = ay * math.pi / 180.0
    msg.linear_acceleration.z = az * math.pi / 180.0

    pub.publish(msg)

def send_battery_message(bat):
    pass

if __name__ == "__main__":
    while True:
        line = serial.readline().decode()
        print(line)

        line = split_line(line)
        if line[0] == "IMU":
            line = map(float, line[1::])
            ax, ay, az, gx, gy, gz, bat = line
            send_imu_msg(ax, ay, az, gx, gy, gz)
            send_battery_message(bat)


   