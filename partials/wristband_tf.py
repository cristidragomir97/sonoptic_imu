import rospy
from sensor_msgs.msg import Imu, MagneticField
from geometry_msgs.msg import Vector3, TransformStamped, Transform, Quaternion
from tf2_ros import TransformBroadcaster
from tf.transformations import *
import numpy as np

ORIGIN = [0.00, 0.5, 0.00]
TOPIC = "/imu/head_raw"

tb = TransformBroadcaster()

def callback(m):
    t = TransformStamped()
    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "fixed"
    t.child_frame_id = "wristband"
    
     self.r = quaternion_from_euler(0, math.pi, math.pi) # sets correct x,y,z
    q = [m.orientation.x, m.orientation.y, m.orientation.z, m.orientation.w]
    
    q_rot = quaternion_from_euler(0, 0, 0)
    q_flip = quaternion_multiply(q, q_rot)
    q_flip  = q_flip  / np.linalg.norm(q_flip)


    t.transform.translation.x  = ORIGIN[0]
    t.transform.translation.y  = ORIGIN[1]
    t.transform.translation.z  = ORIGIN[2]
    t.transform.rotation.x = q_flip[0]
    t.transform.rotation.y = q_flip[1]
    t.transform.rotation.z = q_flip[2]
    t.transform.rotation.w = q_flip[3]        
    tb.sendTransform(t)

    

if __name__ == '__main__':
    rospy.init_node('wrist_tf', anonymous=False)
    rate = rospy.Rate(60)
    rospy.Subscriber("/imu/data", Imu, callback)
    rospy.spin()