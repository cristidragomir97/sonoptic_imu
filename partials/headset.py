import rospy, time, math, serial, signal, sys, tf2_ros
import numpy as np 

from threading import Thread
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, TransformStamped, Transform, Quaternion
from tf2_ros import TransformBroadcaster
from tf.transformations import *
from statistics import mean

def signal_handler(sig, frame):
    sys.exit(0)


ORIGIN = [0.00, 0.5, 0.00]
TOPIC = "/imu/head_raw"



serial = serial.Serial("/dev/ttyS1", 112500, timeout=1)

rospy.init_node('headset_imu', anonymous=False)
rate = rospy.Rate(30)

pub = rospy.Publisher(TOPIC, Imu, queue_size=100)

tb =  tf2_ros.TransformBroadcaster()
tfBuffer = tf2_ros.Buffer()
listener = tf2_ros.TransformListener(tfBuffer)


def send_tf2(q, name):
    t = TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "base_link"
    t.child_frame_id = name
    
    t.transform.translation.x  = ORIGIN[0]
    t.transform.translation.y  = ORIGIN[1] 
    t.transform.translation.z  = ORIGIN[2]
    t.transform.rotation.x = q[0]
    t.transform.rotation.y = q[1]
    t.transform.rotation.z = q[2]
    t.transform.rotation.w = q[3]        
    tb.sendTransform(t)


def check_q(qw, qx, qy, qz):
    q = ((qw ** 2) + (qx ** 2) + (qy ** 2) + (qz ** 2)) 
    print(q)
    return True


def split_line(line):
    return str(line).replace('\r','').replace('\n','').split(',')


def send_tf(qw, qx, qy, qz, rot, name, offset):
    t = TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "base_link"
    t.child_frame_id = name
    
    ## apply transformation to rotate quaternion at 90 degrees
  
    q_rot = quaternion_from_euler(rot[0], rot[1], rot[2])
  
    q_flip = quaternion_multiply(q_rot,[qx, qy, qz, qw], )

    q_flip  = q_flip  / np.linalg.norm(q_flip)

    t.transform.translation.x  = ORIGIN[0]
    t.transform.translation.y  = ORIGIN[1] + offset
    t.transform.translation.z  = ORIGIN[2]
    t.transform.rotation.x = q_flip[0]
    t.transform.rotation.y = q_flip[1]
    t.transform.rotation.z = q_flip[2]
    t.transform.rotation.w = q_flip[3]        
    tb.sendTransform(t)


def get_q():
    line = serial.readline().decode()
    line = split_line(line)
    line = map(float, line)


    qw, qx, qy, qz, la_x, la_y, la_z, gr_x, gr_y, gr_z, bat = line


    return qw, qx, qy, qz
   

def send_battery_message(bat):
    pass


def sample(n):
    for i in range(0, n):
        try:
            qw, qx, qy, qz = get_q()
            send_tf(qw, qx, qy, qz, [0, 0, 0], "original", 0.5)
            time.sleep(0.01)

        except ValueError:
            print("* Misformated message, skiping")

    t = tfBuffer.lookup_transform('base_link', 'original',  rospy.Time(0))
    dx = t.transform.rotation.x 
    dy = t.transform.rotation.y 
    dz = t.transform.rotation.z 
    dw = t.transform.rotation.w 

    return dx, dy, dz, -dw 


def run_in_thread():

    print("* Sampling... ")
    dx, dy, dz, dw = sample(10)

    print(dx, dy, dz, dw)
    #e = euler_from_quaternion([dx, dy, dz, dw])

   # print("* Setting {} as [0, 0, 0]".format(e))
    #Xoff, Yoff, Zoff =  -e[0], -e[1], -e[2]

    print("* Calibration done. Starting loop.. ")
    while True:
        try:
            qw, qx, qy, qz = get_q()
            send_tf(qw, qx, qy, qz, [0, 0, 0], "original", 0.5)

            qq = quaternion_multiply([dx, dy, dz, dw], [qx, qy, qz, qw])
            send_tf(qq[0], qq[1], qq[2], qq[3], [0,0,math.pi], "fixed", 1)

            #send_imu_msg(qw, qx, qy, qz, la_x, la_y, la_z, gr_x, gr_y, gr_z)
            
            #send_battery_message(bat)
        except ValueError:
            print("* Misformated message, skiping")

if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal_handler)

    Thread(target = run_in_thread).start()


   
    

  


   
