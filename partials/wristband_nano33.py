import rospy, sys, struct, time, math 
from bluepy.btle import UUID, Peripheral,DefaultDelegate
from sensor_msgs.msg import Imu, MagneticField
from geometry_msgs.msg import Vector3 


SERVICE_UUID = "590d65c7-3a0a-4023-a05a-6aaf2f22441c"
CHAR_UUID = "00000001-0000-1000-8000-00805f9b34fb"
ADDRESS = "F7:B5:D5:1B:2F:38"

rospy.init_node('hand_imu_raw', anonymous=True)
rate = rospy.Rate(30)


class IMUDelegate(DefaultDelegate):
    def __init__(self, params):
        DefaultDelegate.__init__(self)
        self.raw_pub = rospy.Publisher("/imu/data_raw", Imu, queue_size=100)
        self.mag_pub = rospy.Publisher("/imu/mag", MagneticField, queue_size=10)

        self.gRes = 6.1e-05
        self.aRes = 0.00875
        self.mRes = 0.00875
        self.gOff = [-0.05, 0.35, -0.73]
        self.aOff = [0.04, -0.1, -0.01]
        self.mOff = [-0.00563, 0.01678, -0.0068]
        self.soft_iron = [0.99, -0.002, -0.005, -0.002, 0.989, -0.075, -0.005, -0.075, 1.027]
    
    def make_message(self, float_list):
        #print(float_list)
        mx, my, mz, ax, ay, az, gx, gy, gz = float_list
        
        raw_msg = Imu()
        raw_msg.header.frame_id = "hand"
        raw_msg.header.stamp = rospy.get_rostime()

        raw_msg.angular_velocity.x = gx * math.pi / 180.0
        raw_msg.angular_velocity.y = gy * math.pi / 180.0
        raw_msg.angular_velocity.z = gz * math.pi / 180.0
        
        raw_msg.linear_acceleration.x = ax * math.pi / 180.0
        raw_msg.linear_acceleration.y = ay * math.pi / 180.0
        raw_msg.linear_acceleration.z = az * math.pi / 180.0

        mag_msg = MagneticField()
        mag_msg.header.frame_id = "hand"
        mag_msg.header.stamp = rospy.get_rostime()

        mag_msg.magnetic_field.x = mx
        mag_msg.magnetic_field.y = my
        mag_msg.magnetic_field.z = mx
    

        #msg.angular_velocity.x = (gx *  self.gRes - self.gOff[0]) * math.pi / 180.0
        #msg.angular_velocity.y = (gy *  self.gRes - self.gOff[1]) * math.pi / 180.0
        #msg.angular_velocity.z = (gz *  self.gRes - self.gOff[2]) * math.pi / 180.0
        #msg.angular_velocity_covariance[0] = self.gOff[0] * self.gOff[0]
        #msg.angular_velocity_covariance[4] = self.gOff[1] * self.gOff[1]
        #msg.angular_velocity_covariance[8] = self.gOff[2] * self.gOff[2]
        #msg.linear_acceleration.x = (ax *  self.aRes - self.aOff[0]) * math.pi / 180.0
        #msg.linear_acceleration.y = (ay *  self.aRes - self.aOff[1]) * math.pi / 180.0
        #msg.linear_acceleration.z = (az *  self.aRes - self.aOff[2]) * math.pi / 180.0
        #msg.linear_acceleration_covariance[0] = self.aOff[0] * self.aOff[0]
        #msg.linear_acceleration_covariance[4] = self.aOff[1] * self.aOff[1]
        #msg.linear_acceleration_covariance[8] = self.aOff[2] * self.aOff[2]

        return mag_msg, raw_msg




    def handleNotification(self, cHandle, data):
        line = data.decode().split(",")
        float_list = list(map(float, line))
        mag_msg, raw_msg = self.make_message(float_list)
        
        self.raw_pub.publish(raw_msg)
        self.mag_pub.publish(mag_msg)


def setup_ble():
    imu_service_uuid = UUID(SERVICE_UUID)
    imu_char_uuid    = UUID(CHAR_UUID)

    p = Peripheral(ADDRESS)
    service = p.getServiceByUUID(imu_service_uuid)
    characteristic = service.getCharacteristics(imu_char_uuid)[0]

    # set buffer size 
    time.sleep(1)
    p.setMTU(244)

    # get handle and turn notifications on 
    handle = p.getDescriptors(characteristic.getHandle(),0x0C)[0].handle
    p.writeCharacteristic(characteristic.valHandle+1, struct.pack('<bb', 0x02, 0x00))

    return p

if __name__ == "__main__":
    device = setup_ble()
    device.setDelegate(IMUDelegate(device))

    while True:
        if device.waitForNotifications(0.01):
            continue
        

    
    
  