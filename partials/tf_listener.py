import rospy
import math
import tf2_ros
import geometry_msgs.msg


if __name__ == '__main__':
    rospy.init_node('tfplm')
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)
    rate = rospy.Rate(30.0)

    while not rospy.is_shutdown():
        try:
            sub = tfBuffer.lookup_transform('base_link', 'original',  rospy.Time(0))
            print(sub)
        except Exception as e:
            print(e)
            rate.sleep()
            continue