import rospy, signal, sys, subprocess
from headset import HeadsetReceiver
from wristband import WristReceiver, WristBroadcaster

from colorama import Fore

rospy.init_node('imu', anonymous=False)
rate = rospy.Rate(30)


def signal_handler(sig, frame):
    subprocess.call("kill {}".format(filter_pid), shell=True)
    sys.exit()

calib_flag = "OFF"
filter_pid = 0

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)

    HeadsetReceiver().start()
    WristReceiver().start()
    WristBroadcaster().start()
    
    signal.pause()