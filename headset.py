
import rospy, time, math, serial, signal, sys, tf2_ros, threading
import numpy as np 

from threading import Thread
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, TransformStamped, Transform, Quaternion
from tf2_ros import TransformBroadcaster
from tf.transformations import *
from colorama import Fore


def split_line(line):
    return str(line).replace('\r','').replace('\n','').split(',')

def magic_transform(q):
    return q[1], q[2], q[3], q[0]

class HeadsetReceiver(threading.Thread):

    def __init__(self):
        print(Fore.YELLOW + "[*] Headset reciever started")
        print(Fore.YELLOW + "\t * Setting current position as origin")


        self.origin = [0.00, 0.5, 0.00]
        self.serial = serial.Serial("/dev/ttyS1", 112500, timeout=1)
        self.caster =  tf2_ros.TransformBroadcaster()
        self.buffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.buffer)
        self.d = self.sample(10)
        self.r = quaternion_from_euler(0, math.pi, math.pi) # sets correct x,y,z

        print(Fore.YELLOW + "\t * Calibration done. \n\t * Starting loop... ")
        threading.Thread.__init__(self)

    def sample(self, n):
        for i in range(0, n):
            try:
                q = self.get_q()
                self.send_tf(q, "original")
                time.sleep(0.01)

            except ValueError:
                print(Fore.RED + "* Misformated message, skiping")

        t = self.buffer.lookup_transform('base_link', 'original',  rospy.Time(0))
        dx = t.transform.rotation.x 
        dy = t.transform.rotation.y 
        dz = t.transform.rotation.z 
        dw = t.transform.rotation.w 
        return [dx, dy, dz, -dw]
    
    def send_tf(self, q, name):
        t = TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "base_link"
        t.child_frame_id = name
        t.transform.translation.x  = self.origin[0]
        t.transform.translation.y  = self.origin[1] 
        t.transform.translation.z  = self.origin[2]
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]        
        self.caster.sendTransform(t)

    def get_q(self):
        line = self.serial.readline().decode()
        line = split_line(line)
        line = map(float, line)
        qw, qx, qy, qz, la_x, la_y, la_z, gr_x, gr_y, gr_z, bat = line
        q = [qx, qy, qz, qw]
        
        return  q / np.linalg.norm(q)

    def run(self):
        while True:
            q = self.get_q()
            q = quaternion_multiply(self.d, q)
            q = quaternion_multiply(self.r, q)
            q = magic_transform(q) 
            self.send_tf(q, 'headset')




   
       



   
    

  


   
