

if __name__ == "__main__":

    t = self.buffer.lookup_transform('base_link', 'original',  rospy.Time(0))
        
        dx = t.transform.rotation.x 
        dy = t.transform.rotation.y 
        dz = t.transform.rotation.z 
        dw = t.transform.rotation.w 
        return [dx, dy, dz, -dw]